package com.proop.lab4.shape;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;

import java.util.List;

import static java.util.Arrays.asList;

public class Circle implements Shape {

    public static final String SHAPE_NAME = "circle";

    private Point center;
    private Line radius;

    @JsonCreator
    public Circle(@JsonProperty("center") Point center, @JsonProperty("radius") Line radius) {
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Line getRadius() {
        return radius;
    }

    public void setRadius(Line radius) {
        this.radius = radius;
    }

    @Override
    public List<Point> getPoints() {
        return asList(center);
    }

    @Override
    public void setPoints(List<Point> points) {
        center = points.get(0);
    }

    @Override
    public List<Line> getLines() {
        return asList(radius);
    }

    @Override
    public void setLines(List<Line> lines) {
        radius = lines.get(0);
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[] {center.getX(), center.getY(), radius.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }
}
