package com.proop.lab4.shape.factory;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.Segment;

import java.util.List;

public class SegmentFactory implements ShapeFactory<Segment> {

    @Override
    public Segment createShape(List<Point> points, List<Line> lines) {
        return new Segment(points.get(0), points.get(1));
    }

}
