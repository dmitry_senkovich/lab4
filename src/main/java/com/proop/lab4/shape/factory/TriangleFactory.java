package com.proop.lab4.shape.factory;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.Triangle;

import java.util.List;

public class TriangleFactory implements ShapeFactory<Triangle> {

    @Override
    public Triangle createShape(List<Point> points, List<Line> lines) {
        return new Triangle(points.get(0), points.get(1), points.get(2));
    }

}
