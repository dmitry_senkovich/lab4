package com.proop.lab4.shape.factory.abstractfactory;

import com.proop.lab4.abstractfactory.AbstractFactory;
import com.proop.lab4.shape.Shape;
import com.proop.lab4.shape.factory.ShapeFactory;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactoryAbstractFactory implements AbstractFactory<ShapeFactory> {

    static private Map<Class<? extends Shape>, ShapeFactory> shapeFactories = new HashMap<>();
    static private ShapeFactoryAbstractFactory SHAPE_FACTORY_ABSTRACT_FACTORY = new ShapeFactoryAbstractFactory();

    private ShapeFactoryAbstractFactory() {}

    @Override
    public void registerShape(Class shapeClass, ShapeFactory shapeFactory) {
        shapeFactories.put(shapeClass, shapeFactory);
    }

    @Override
    public ShapeFactory getFactoryObject(Class shapeClass) {
        return shapeFactories.get(shapeClass);
    }

    public static ShapeFactoryAbstractFactory getInstance() {
        return SHAPE_FACTORY_ABSTRACT_FACTORY;
    }

}
