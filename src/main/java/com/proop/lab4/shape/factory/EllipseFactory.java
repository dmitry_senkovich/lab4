package com.proop.lab4.shape.factory;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.Ellipse;

import java.util.List;

public class EllipseFactory implements ShapeFactory<Ellipse> {

    @Override
    public Ellipse createShape(List<Point> points, List<Line> lines) {
        return new Ellipse(points.get(0), lines.get(0), lines.get(1));
    }

}
