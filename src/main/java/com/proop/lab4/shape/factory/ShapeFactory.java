package com.proop.lab4.shape.factory;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.Shape;

import java.util.List;

public interface ShapeFactory<S extends Shape> {

    S createShape(List<Point> points, List<Line> lines);

}
