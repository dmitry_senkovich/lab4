package com.proop.lab4.shape;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class Triangle implements Shape {

    public static final String SHAPE_NAME = "triangle";

    private Point firstVertex;
    private Point secondVertex;
    private Point thirdVertex;

    @JsonCreator
    public Triangle(@JsonProperty("firstVertex") Point firstVertex,
                    @JsonProperty("secondVertex") Point secondVertex,
                    @JsonProperty("thirdVertex") Point thirdVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        this.thirdVertex = thirdVertex;
    }

    public Point getFirstVertex() {
        return firstVertex;
    }

    public void setFirstVertex(Point firstVertex) {
        this.firstVertex = firstVertex;
    }

    public Point getSecondVertex() {
        return secondVertex;
    }

    public void setSecondVertex(Point secondVertex) {
        this.secondVertex = secondVertex;
    }

    public Point getThirdVertex() {
        return thirdVertex;
    }

    public void setThirdVertex(Point thirdVertex) {
        this.thirdVertex = thirdVertex;
    }

    @Override
    public List<Point> getPoints() {
        return asList(firstVertex, secondVertex, thirdVertex);
    }

    @Override
    public void setPoints(List<Point> points) {
        firstVertex = points.get(0);
        secondVertex = points.get(1);
        thirdVertex = points.get(2);
    }

    @Override
    public List<Line> getLines() {
        return emptyList();
    }

    @Override
    public void setLines(List<Line> lines) {
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{firstVertex.getX(), firstVertex.getY(), secondVertex.getX(), secondVertex.getY(), thirdVertex.getX(), thirdVertex.getY()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }
}
