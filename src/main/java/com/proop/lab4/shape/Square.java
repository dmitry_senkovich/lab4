package com.proop.lab4.shape;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;

import java.util.List;

import static java.util.Arrays.asList;

public class Square implements Shape {

    public static final String SHAPE_NAME = "square";

    private Point topLeftVertex;
    private Line side;

    @JsonCreator
    public Square(@JsonProperty("topLeftVertex") Point topLeftVertex, @JsonProperty("side") Line side) {
        this.topLeftVertex = topLeftVertex;
        this.side = side;
    }

    public Point getTopLeftVertex() {
        return topLeftVertex;
    }

    public void setTopLeftVertex(Point topLeftVertex) {
        this.topLeftVertex = topLeftVertex;
    }

    public Line getSide() {
        return side;
    }

    public void setSide(Line side) {
        this.side = side;
    }

    @Override
    public List<Point> getPoints() {
        return asList(topLeftVertex);
    }

    @Override
    public void setPoints(List<Point> points) {
        topLeftVertex = points.get(0);
    }

    @Override
    public List<Line> getLines() {
        return asList(side);
    }

    @Override
    public void setLines(List<Line> lines) {
        side = lines.get(0);
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{topLeftVertex.getX(), topLeftVertex.getY(), side.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }

}
