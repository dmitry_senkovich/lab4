package com.proop.lab4.shape;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;

import java.util.List;

import static java.util.Arrays.asList;

public class Ellipse implements Shape {

    public static final String SHAPE_NAME = "ellipse";

    private Point center;
    private Line semiMinorAxis;
    private Line semiMajorAxis;

    @JsonCreator
    public Ellipse(@JsonProperty("center") Point center,
                   @JsonProperty("semiMinorAxis") Line semiMinorAxis,
                   @JsonProperty("semiMajorAxis") Line semiMajorAxis) {
        this.center = center;
        this.semiMinorAxis = semiMinorAxis;
        this.semiMajorAxis = semiMajorAxis;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Line getSemiMinorAxis() {
        return semiMinorAxis;
    }

    public void setSemiMinorAxis(Line semiMinorAxis) {
        this.semiMinorAxis = semiMinorAxis;
    }

    public Line getSemiMajorAxis() {
        return semiMajorAxis;
    }

    public void setSemiMajorAxis(Line semiMajorAxis) {
        this.semiMajorAxis = semiMajorAxis;
    }

    @Override
    public List<Point> getPoints() {
        return asList(center);
    }

    @Override
    public void setPoints(List<Point> points) {
        center = points.get(0);
    }

    @Override
    public List<Line> getLines() {
        return asList(semiMinorAxis, semiMajorAxis);
    }

    @Override
    public void setLines(List<Line> lines) {
        semiMinorAxis = lines.get(0);
        semiMajorAxis = lines.get(1);
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{center.getX(), center.getY(), semiMinorAxis.getLength(), semiMajorAxis.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }

}
