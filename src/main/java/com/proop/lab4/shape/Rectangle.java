package com.proop.lab4.shape;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;

import java.util.List;

import static java.util.Arrays.asList;

public class Rectangle implements Shape {

    public static final String SHAPE_NAME = "rectangle";

    private Point topLeftVertex;
    private Line verticalSide;
    private Line horizontalSide;

    @JsonCreator
    public Rectangle(@JsonProperty("topLeftVertex") Point topLeftVertex,
                     @JsonProperty("verticalSide") Line verticalSide,
                     @JsonProperty("horizontalSide") Line horizontalSide) {
        this.topLeftVertex = topLeftVertex;
        this.verticalSide = verticalSide;
        this.horizontalSide = horizontalSide;
    }

    public Point getTopLeftVertex() {
        return topLeftVertex;
    }

    public void setTopLeftVertex(Point topLeftVertex) {
        this.topLeftVertex = topLeftVertex;
    }

    public Line getVerticalSide() {
        return verticalSide;
    }

    public void setVerticalSide(Line verticalSide) {
        this.verticalSide = verticalSide;
    }

    public Line getHorizontalSide() {
        return horizontalSide;
    }

    public void setHorizontalSide(Line horizontalSide) {
        this.horizontalSide = horizontalSide;
    }

    @Override
    public List<Point> getPoints() {
        return asList(topLeftVertex);
    }

    @Override
    public void setPoints(List<Point> points) {
        topLeftVertex = points.get(0);
    }

    @Override
    public List<Line> getLines() {
        return asList(verticalSide, horizontalSide);
    }

    @Override
    public void setLines(List<Line> lines) {
        verticalSide = lines.get(0);
        horizontalSide = lines.get(1);
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{topLeftVertex.getX(), topLeftVertex.getY(), verticalSide.getLength(), horizontalSide.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }
}
