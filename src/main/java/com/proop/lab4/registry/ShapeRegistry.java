package com.proop.lab4.registry;

import com.proop.lab4.shape.Shape;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ShapeRegistry {

    private static final ShapeRegistry SHAPE_REGISTRY = new ShapeRegistry();

    private List<String> shapeNames;
    private Set<Class<? extends Shape>> shapeClasses;
    private Map<String, Class<? extends Shape>> shapes;

    private ShapeRegistry() {
        shapeClasses = new Reflections(ClasspathHelper.forJavaClassPath(), ClasspathHelper.forManifest(), new SubTypesScanner()).getSubTypesOf(Shape.class);
        shapeNames = shapeClasses.stream().map(this::getShapeName).collect(Collectors.toList());
        shapes = shapeClasses.stream().collect(Collectors.toMap(this::getShapeName, Function.identity()));
    }

    private String getShapeName(Class<? extends Shape> shapeClass) {
        String shapeName = null;
        try {
            shapeName = (String) shapeClass.getField("SHAPE_NAME").get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            System.out.println("Failed to get shape name for shape class: " + shapeClass.getSimpleName());
        }

        return shapeName;
    }

    public List<String> getRegisteredShapeNames() {
        return shapeNames;
    }

    public Set<Class<? extends Shape>> getRegisteredShapeClasses() {
        return shapeClasses;
    }

    public Map<String, Class<? extends Shape>> getRegisteredShapes() {
        return shapes;
    }

    public static ShapeRegistry getInstance() {
        return SHAPE_REGISTRY;
    }

}
