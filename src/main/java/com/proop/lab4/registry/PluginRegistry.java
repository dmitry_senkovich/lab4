package com.proop.lab4.registry;

import com.proop.lab4.plugin.ShapePlugin;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PluginRegistry {

    private static final PluginRegistry PLUGIN_REGISTRY = new PluginRegistry();

    private List<ShapePlugin> shapePlugins;

    private PluginRegistry() {
        shapePlugins = new Reflections(ClasspathHelper.forJavaClassPath(), ClasspathHelper.forManifest(), new SubTypesScanner())
                .getSubTypesOf(ShapePlugin.class).stream().map(pluginClass -> {
                    ShapePlugin plugin = null;
                    try {
                        plugin = pluginClass.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                        System.out.println("Failed to load plugin: " + pluginClass.getSimpleName());
                    }

                    return plugin;
                }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<ShapePlugin> getShapePlugins() {
        return shapePlugins;
    }

    public static PluginRegistry getInstance() {
        return PLUGIN_REGISTRY;
    }
}
