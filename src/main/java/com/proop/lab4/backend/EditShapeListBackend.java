package com.proop.lab4.backend;

import com.proop.lab4.helper.ShapeContainer;
import com.proop.lab4.helper.ShapeContainerPainter;
import com.proop.lab4.json.JsonMarshaller;
import com.proop.lab4.json.JsonUnmarshaller;
import com.proop.lab4.painter.factory.ShapePainterFactory;
import com.proop.lab4.shape.Shape;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class EditShapeListBackend {

    private JsonMarshaller jsonMarshaller = new JsonMarshaller();
    private JsonUnmarshaller jsonUnmarshaller = new JsonUnmarshaller();
    private File file;

    private ShapePainterFactory shapePainterFactory = ShapePainterFactory.getInstance();
    private ShapeContainerPainter shapeContainerPainter = new ShapeContainerPainter();

    private ShapeContainer shapeContainer = new ShapeContainer();
    private List<String> shapesPainted;

    private int currentShapeIndex = 0;

    public EditShapeListBackend(File file) throws IOException {
        this.file = file;
        try {
            shapeContainer.setShapes(jsonUnmarshaller.read(file));
            if (shapeContainer.getShapes().isEmpty()) {
                throw new IOException("No shapes found in file: " + file.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to load shapes from file: " + file.getName());
            throw e;
        }
        shapesPainted = shapeContainerPainter.drawEachShape(shapeContainer);
    }

    public void updateShapesPainted() {
        shapesPainted = shapeContainerPainter.drawEachShape(shapeContainer);
    }

    public List<String> getShapesPainted() {
        return shapesPainted;
    }

    public ShapeContainer getShapeContainer() {
        return shapeContainer;
    }

    public void setCurrentShapeIndex(int currentShapeIndex) {
        this.currentShapeIndex = currentShapeIndex;
    }

    public int getCurrentShapeIndex() {
        return currentShapeIndex;
    }

    public Shape getCurrentShape() {
        return shapeContainer.getShapes().get(currentShapeIndex);
    }

    public void removeCurrentShape() {
        shapesPainted.remove(currentShapeIndex);
        shapeContainer.getShapes().remove(currentShapeIndex);
    }

    public void saveShapeList() throws IOException {
        jsonMarshaller.save(file, shapeContainer.getShapes());
    }

}
