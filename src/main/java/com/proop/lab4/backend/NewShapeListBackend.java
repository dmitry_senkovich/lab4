package com.proop.lab4.backend;

import com.proop.lab4.helper.ShapeContainer;
import com.proop.lab4.helper.ShapeContainerPainter;
import com.proop.lab4.json.JsonMarshaller;
import com.proop.lab4.painter.factory.ShapePainterFactory;

import java.io.File;
import java.io.IOException;

public class NewShapeListBackend {

    private ShapePainterFactory shapePainterFactory = ShapePainterFactory.getInstance();
    private ShapeContainerPainter shapeContainerPainter = new ShapeContainerPainter();

    private ShapeContainer shapeContainer = new ShapeContainer();

    private JsonMarshaller jsonMarshaller = new JsonMarshaller();

    public void setShapeContainer(ShapeContainer shapeContainer) {
        this.shapeContainer = shapeContainer;
    }

    public ShapeContainer getShapeContainer() {
        return this.shapeContainer;
    }

    public void resetCurrentShapes() {
        shapeContainer.getShapes().clear();
    }

    public String drawShapes() {
        return shapeContainerPainter.drawShapes(shapeContainer);
    }

    public void saveShapes(File file) throws IOException {
        jsonMarshaller.save(file, shapeContainer.getShapes());
    }

}
