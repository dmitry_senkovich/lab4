package com.proop.lab4.initializer;

public abstract class AbstractInitializer implements Initializer {

    protected Object createObjectInstance(Class classInstanceOfToCreate) {
        Object objectToInstantiate = null;
        try {
            objectToInstantiate = classInstanceOfToCreate.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            System.out.println("Failed to instantiate instance of class: " + classInstanceOfToCreate.getSimpleName());
        }

        return objectToInstantiate;
    }

}
