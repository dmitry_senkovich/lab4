package com.proop.lab4.initializer;

import com.proop.lab4.registry.ShapeRegistry;
import com.proop.lab4.painter.ShapePainter;
import com.proop.lab4.painter.factory.ShapePainterFactory;

import org.apache.commons.text.WordUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;

import java.util.Set;

public class ShapePainterFactoryInitializer extends AbstractInitializer {

    static private ShapePainterFactoryInitializer SHAPE_PAINTER_FACTORY_INITIALIZER = new ShapePainterFactoryInitializer();

    private ShapeRegistry shapeRegistry = ShapeRegistry.getInstance();

    private ShapePainterFactoryInitializer() {}

    @Override
    public void init() {
        ShapePainterFactory shapePainterFactory = ShapePainterFactory.getInstance();
        Set<Class<? extends ShapePainter>> shapePainterClasses = new Reflections(ClasspathHelper.forJavaClassPath(), ClasspathHelper.forManifest(), new SubTypesScanner()).getSubTypesOf(ShapePainter.class);
        shapeRegistry.getRegisteredShapeNames().forEach(shapeName -> {
            Class<? extends ShapePainter> shapePainterClass
                    = shapePainterClasses.stream()
                    .filter(shapePainterClazz -> shapePainterClazz.getSimpleName()
                            .equals(WordUtils.capitalize(shapeName) + "Painter"))
                    .findFirst().get();

            shapePainterFactory.registerShape(shapeRegistry.getRegisteredShapes().get(shapeName),
                    (ShapePainter) createObjectInstance(shapePainterClass));
        });
    }

    public static ShapePainterFactoryInitializer getInstance() {
        return SHAPE_PAINTER_FACTORY_INITIALIZER;
    }

}
