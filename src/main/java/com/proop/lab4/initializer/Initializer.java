package com.proop.lab4.initializer;

public interface Initializer {
    void init();
}
