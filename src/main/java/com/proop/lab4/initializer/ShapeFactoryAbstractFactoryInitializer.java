package com.proop.lab4.initializer;

import com.proop.lab4.registry.ShapeRegistry;
import com.proop.lab4.shape.factory.ShapeFactory;
import com.proop.lab4.shape.factory.abstractfactory.ShapeFactoryAbstractFactory;

import org.apache.commons.text.WordUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;

import java.util.Set;

public class ShapeFactoryAbstractFactoryInitializer extends AbstractInitializer {

    static private ShapeFactoryAbstractFactoryInitializer SHAPE_FACTORY_ABSTRACT_FACTORY_INITIALIZER = new ShapeFactoryAbstractFactoryInitializer();

    private ShapeRegistry shapeRegistry = ShapeRegistry.getInstance();

    private ShapeFactoryAbstractFactoryInitializer() {}

    @Override
    public void init() {
        ShapeFactoryAbstractFactory shapeFactoryAbstractFactory = ShapeFactoryAbstractFactory.getInstance();
        Set<Class<? extends ShapeFactory>> shapeFactoryClasses = new Reflections(ClasspathHelper.forJavaClassPath(), ClasspathHelper.forManifest(), new SubTypesScanner()).getSubTypesOf(ShapeFactory.class);
        shapeRegistry.getRegisteredShapeNames().forEach(shapeName -> {
            Class<? extends ShapeFactory> shapeFactoryClass
                    = shapeFactoryClasses.stream()
                                         .filter(shapeFactoryClazz -> shapeFactoryClazz.getSimpleName()
                                                 .equals(WordUtils.capitalize(shapeName) + "Factory"))
                                         .findFirst().get();

            shapeFactoryAbstractFactory.registerShape(shapeRegistry.getRegisteredShapes().get(shapeName),
                    (ShapeFactory) createObjectInstance(shapeFactoryClass));
        });
    }

    public static ShapeFactoryAbstractFactoryInitializer getInstance() {
        return SHAPE_FACTORY_ABSTRACT_FACTORY_INITIALIZER;
    }

}
