package com.proop.lab4.frontend;

import com.proop.lab4.initializer.ShapeFactoryAbstractFactoryInitializer;
import com.proop.lab4.initializer.ShapePainterFactoryInitializer;
import com.proop.lab4.plugin.ShapeMenu;
import com.proop.lab4.registry.PluginRegistry;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public class AppFrame extends JFrame {

    public AppFrame() {
        ShapeFactoryAbstractFactoryInitializer.getInstance().init();
        ShapePainterFactoryInitializer.getInstance().init();

        JMenuBar menuBar = createMenuBar();

        this.setTitle("Lab 4");
        this.add(new NewShapeListForm());
        this.setJMenuBar(menuBar);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.add(createNewMenuItem());
        fileMenu.add(createLoadMenuItem());
        menuBar.add(fileMenu);

        applyPlugins(menuBar);

        return menuBar;
    }

    private JMenuItem createNewMenuItem() {
        JFrame thisJFrame = this;
        JMenuItem newMenuItem = new JMenuItem("New");
        newMenuItem.addActionListener(e -> {
            thisJFrame.getContentPane().removeAll();
            thisJFrame.add(new NewShapeListForm());
            thisJFrame.pack();
            thisJFrame.revalidate();
        });

        return newMenuItem;
    }

    private JMenuItem createLoadMenuItem() {
        JFrame thisJFrame = this;
        JMenuItem loadMenuItem = new JMenuItem("Load");
        loadMenuItem.addActionListener(e -> {
            JFileChooser jFileChooser = new JFileChooser();
            int returnValue = jFileChooser.showOpenDialog(this);
            if (APPROVE_OPTION == returnValue) {
                thisJFrame.getContentPane().removeAll();
                File file = jFileChooser.getSelectedFile();
                try {
                    thisJFrame.add(new EditShapeListForm(file));
                } catch (IOException exception) {
                    System.out.println("Failed to load shapes from file: " + file.getName());
                    thisJFrame.add(new NewShapeListForm());
                }
                thisJFrame.pack();
                thisJFrame.revalidate();
            }
        });

        return loadMenuItem;
    }

    private void applyPlugins(JMenuBar menuBar) {
        PluginRegistry.getInstance().getShapePlugins().forEach(plugin ->  {
            plugin.getShapeMenus().forEach(shapeMenuClass -> {
                try {
                    ShapeMenu pluginMenu = shapeMenuClass.newInstance();
                    pluginMenu.setParentFrame(this);
                    menuBar.add(pluginMenu);
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                    System.out.println("Failed to add menu for plugin: " + plugin.getClass().getSimpleName());
                }
            });
        });
    }

}
