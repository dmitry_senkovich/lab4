/*
 * Created by JFormDesigner on Sun Mar 11 23:18:32 MSK 2018
 */

package com.proop.lab4.frontend;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.backend.NewShapeBackend;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author Dmitry Senkovich
 */
public class NewShapeForm extends JPanel {

    private NewShapeBackend newShapeBackend = new NewShapeBackend();

    public NewShapeForm() {
        initForm();
    }

    public NewShapeForm(NewShapeBackend newShapeBackend) {
        this.newShapeBackend = newShapeBackend;
        initForm();
    }

    private void initForm() {
        initComponents();
        // layout hack
        setBounds(0, 0, 400, 385);
        shapeComboBox.setModel(new DefaultComboBoxModel(newShapeBackend.getShapeNames().toArray()));
        resetCurrentShapeName();
    }

    private void resetCurrentShapeName() {
        newShapeBackend.setCurrentShapeName(newShapeBackend.getShapeNames().get(0));
    }

    private void shapeComboBoxItemStateChanged(ItemEvent e) {
        newShapeBackend.setCurrentShapeName(shapeComboBox.getSelectedItem().toString());
    }

    private void addPointButtonMousePressed(MouseEvent e) {
        try {
            Integer x = Integer.valueOf(xTextField.getText());
            Integer y = Integer.valueOf(yTextField.getText());
            com.proop.lab4.asset.Point point = new Point(x, y);
            newShapeBackend.addPoint(point);
            xTextField.setText("");
            yTextField.setText("");
            currentPointsTextArea.setText(newShapeBackend.getCurrentPoints());
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(this, "Failed to add a new point. Make sure x and y are valid",
                    "Error while adding a new point",  JOptionPane.ERROR_MESSAGE);
            System.out.println("Failed to add a new point");
        }
    }

    private void addLineButtonMousePressed(MouseEvent e) {
        try {
            Integer length = Integer.valueOf(lengthTextField.getText());
            Line line = new Line(length);
            newShapeBackend.addLine(line);
            lengthTextField.setText("");
            currentLinesTextArea.setText(newShapeBackend.getCurrentLines());
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(this, "Failed to add a new line. Make sure length is valid",
                    "Error while adding a new line",  JOptionPane.ERROR_MESSAGE);
            System.out.println("Failed to add a new line");
        }
    }

    private void resetCurrentShapeButtonMousePressed(MouseEvent e) {
        resetCurrentShape();
    }

    public void resetCurrentShape() {
        newShapeBackend.resetCurrentShape();
        resetCurrentShapeName();
        shapeComboBox.setSelectedIndex(0);
        xTextField.setText("");
        yTextField.setText("");
        lengthTextField.setText("");
        currentPointsTextArea.setText("");
        currentLinesTextArea.setText("");
    }

    private void addShapeButtonMousePressed(MouseEvent e) {
        boolean created = newShapeBackend.createShape();
        if (created) {
            resetCurrentShape();
            JOptionPane.showMessageDialog(this, "Shape is created!");
        } else {
            JOptionPane.showMessageDialog(this,
                    "Failed to create a new shape. Please, recheck current coordinates",
                    "Error while creating a shape",  JOptionPane.ERROR_MESSAGE);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
        xTextField = new JTextField();
        yTextField = new JTextField();
        shapeComboBox = new JComboBox();
        addPointButton = new JButton();
        lengthTextField = new JTextField();
        addLineButton = new JButton();
        addShapeButton = new JButton();
        currentPointsTextArea = new JTextArea();
        currentLinesTextArea = new JTextArea();
        resetCurrentShapeButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(400, 385));
        setMaximumSize(new Dimension(400, 385));
        setPreferredSize(new Dimension(400, 385));

        setLayout(null);
        add(xTextField);
        xTextField.setBounds(10, 55, 75, 30);
        add(yTextField);
        yTextField.setBounds(100, 55, 75, 30);

        //---- shapeComboBox ----
        shapeComboBox.addItemListener(e -> shapeComboBoxItemStateChanged(e));
        add(shapeComboBox);
        shapeComboBox.setBounds(10, 20, 100, shapeComboBox.getPreferredSize().height);

        //---- addPointButton ----
        addPointButton.setText("Add Point");
        addPointButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addPointButtonMousePressed(e);
            }
        });
        add(addPointButton);
        addPointButton.setBounds(205, 55, 105, 30);
        add(lengthTextField);
        lengthTextField.setBounds(10, 205, 75, 30);

        //---- addLineButton ----
        addLineButton.setText("Add Line");
        addLineButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addLineButtonMousePressed(e);
            }
        });
        add(addLineButton);
        addLineButton.setBounds(205, 205, 105, 30);

        //---- addShapeButton ----
        addShapeButton.setText("Add Shape");
        addShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addShapeButtonMousePressed(e);
            }
        });
        add(addShapeButton);
        addShapeButton.setBounds(205, 340, 105, 30);

        //---- currentPointsTextArea ----
        currentPointsTextArea.setEnabled(false);
        currentPointsTextArea.setEditable(false);
        add(currentPointsTextArea);
        currentPointsTextArea.setBounds(10, 105, 373, 78);

        //---- currentLinesTextArea ----
        currentLinesTextArea.setEnabled(false);
        currentLinesTextArea.setEditable(false);
        add(currentLinesTextArea);
        currentLinesTextArea.setBounds(10, 250, 373, 78);

        //---- resetCurrentShapeButton ----
        resetCurrentShapeButton.setText("Reset");
        resetCurrentShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                resetCurrentShapeButtonMousePressed(e);
            }
        });
        add(resetCurrentShapeButton);
        resetCurrentShapeButton.setBounds(55, 340, 105, 30);

        setPreferredSize(new Dimension(400, 385));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
    private JTextField xTextField;
    private JTextField yTextField;
    private JComboBox shapeComboBox;
    private JButton addPointButton;
    private JTextField lengthTextField;
    private JButton addLineButton;
    private JButton addShapeButton;
    private JTextArea currentPointsTextArea;
    private JTextArea currentLinesTextArea;
    private JButton resetCurrentShapeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
