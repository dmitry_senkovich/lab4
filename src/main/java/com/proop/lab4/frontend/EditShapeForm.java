package com.proop.lab4.frontend;

import com.google.common.collect.Streams;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.Shape;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class EditShapeForm extends JPanel {

    private Shape shape;

    public EditShapeForm(Shape shape) {
        this.shape = shape;
        initComponents();
    }

    public void saveChangesButtonMousePressed(MouseEvent e) {
        if (shapeChangesAreValid()) {
            savePointsChanges();
            saveLinesChanges();
            // or else windows closed event is not fired
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(new WindowEvent(SwingUtilities.getWindowAncestor(this), WindowEvent.WINDOW_CLOSING));
        } else {
            JOptionPane.showMessageDialog(this, "Failed to change shape. Make sure all the coordinates are valid",
                    "Error while changing shape",  JOptionPane.ERROR_MESSAGE);
            System.out.println("Failed to change shape");
        }
    }

    private boolean shapeChangesAreValid() {
        boolean valid = textFieldAreNumeric(xTextFields);
        valid = valid && textFieldAreNumeric(yTextFields);
        valid = valid && textFieldAreNumeric(lengthTextFields);
        return valid;
    }

    private boolean textFieldAreNumeric(List<JTextField> textFields) {
        return textFields.stream().allMatch(textField -> StringUtils.isNumeric(textField.getText()));
    }

    private void savePointsChanges() {
        List<Point> newPoints = Streams.zip(xTextFields.stream(), yTextFields.stream(),
                (xTextField, yTextField) -> new Point(NumberUtils.toInt(xTextField.getText()), NumberUtils.toInt(yTextField.getText())))
        .collect(Collectors.toList());
        shape.setPoints(newPoints);
    }

    private void saveLinesChanges() {
        List<Line> newLines = lengthTextFields.stream()
                .map(lengthTextField -> NumberUtils.toInt(lengthTextField.getText()))
                .map(Line::new)
                .collect(Collectors.toList());
        shape.setLines(newLines);
    }

    private void initComponents() {
        xTextFields = new LinkedList<>();
        yTextFields = new LinkedList<>();
        lengthTextFields = new LinkedList<>();

        saveChangesButton = new JButton();

        setMinimumSize(new Dimension(320, 285));
        setMaximumSize(new Dimension(320, 285));
        setPreferredSize(new Dimension(320, 285));

        setLayout(null);

        int xOffset = 10;
        int pointsLabelYOffset = 20;
        int itemHeight = 20;
        int itemWidth = 55;
        int cellHeight = 40;

        JLabel pointsLabel = new JLabel("Points:");
        pointsLabel.setBounds(xOffset, pointsLabelYOffset, itemWidth, itemHeight);
        add(pointsLabel);

        int pointsYOffset = pointsLabelYOffset + cellHeight;

        IntStream.range(0, shape.getPoints().size())
                .forEach(i -> {
                    int yOffset = pointsYOffset + i*(cellHeight);
                    Point point = shape.getPoints().get(i);
                    JTextField xTextField = new JTextField(point.getX().toString());
                    xTextField.setBounds(xOffset, yOffset, itemWidth, itemHeight);
                    xTextFields.add(xTextField);
                    add(xTextField);
                    JTextField yTextField = new JTextField(point.getY().toString());
                    yTextField.setBounds(xOffset + xOffset + itemWidth, yOffset, itemWidth, itemHeight);
                    yTextFields.add(yTextField);
                    add(yTextField);
                });

        int linesLabelYOffset = pointsYOffset + shape.getPoints().size()*cellHeight;

        JLabel linesLabel = new JLabel("Lines:");
        linesLabel.setBounds(xOffset, linesLabelYOffset, itemWidth, itemHeight);
        add(linesLabel);

        int linesYOffset = linesLabelYOffset + cellHeight;

        IntStream.range(0, shape.getLines().size())
                .forEach(i -> {
                    int yOffset = linesYOffset + i*cellHeight;
                    Line line = shape.getLines().get(i);
                    JTextField lengthTextField = new JTextField(line.getLength().toString());
                    lengthTextField.setBounds(xOffset, yOffset, itemWidth, itemHeight);
                    lengthTextFields.add(lengthTextField);
                    add(lengthTextField);
                });

        saveChangesButton.setText("Save changes");
        saveChangesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                saveChangesButtonMousePressed(e);
            }
        });
        add(saveChangesButton);
        saveChangesButton.setBounds(new Rectangle(new java.awt.Point(100, 240), saveChangesButton.getPreferredSize()));

        setPreferredSize(new Dimension(320, 285));
    }

    private List<JTextField> xTextFields;
    private List<JTextField> yTextFields;
    private List<JTextField> lengthTextFields;
    private JButton saveChangesButton;
}
