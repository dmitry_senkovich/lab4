/*
 * Created by JFormDesigner on Mon Mar 12 14:18:34 MSK 2018
 */

package com.proop.lab4.frontend;

import com.proop.lab4.backend.EditShapeListBackend;
import com.proop.lab4.backend.NewShapeBackend;
import com.proop.lab4.plugin.ShapeButton;
import com.proop.lab4.plugin.ShapePlugin;
import com.proop.lab4.registry.PluginRegistry;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * @author Dmitry Senkovich
 */
public class EditShapeListForm extends JPanel {

    private PluginRegistry pluginRegistry = PluginRegistry.getInstance();
    private List<ShapeButton> pluginButtons;

    private EditShapeListBackend editShapeListBackend;

    public EditShapeListForm(File file) throws IOException {
        initComponents();
        editShapeListBackend = new EditShapeListBackend(file);
        applyPlugins();
        initForm();
    }

    private void initForm() {
        resetShapeListComboBox();
        resetCurrentShape();
    }

    private void applyPlugins() {
        createPluginButtons();
        updatePlugins();
        placePluginButtons();
    }

    private void createPluginButtons() {
        List<Class<? extends ShapeButton>> pluginButtonClasses = pluginRegistry.getShapePlugins()
                .stream().map(ShapePlugin::getShapeButtons).flatMap(List::stream).collect(Collectors.toList());
        pluginButtons = pluginButtonClasses.stream().map(pluginButtonClass -> {
            ShapeButton shapeButton = null;
            try {
                shapeButton = pluginButtonClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                System.out.println("Failed to add plugin button: " + pluginButtonClass.getSimpleName());
            }
            return shapeButton;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private void placePluginButtons() {
        int xOffset = 10;
        int xPadding = 5;
        int yOffset = 100;
        int yPadding = 5;
        int width = 90;
        int height = 30;
        int buttonCellWidth = width + 2*xPadding;
        int buttonCellHeight = height + 2*yPadding;
        IntStream.range(0, pluginButtons.size()).forEach(i -> {
            ShapeButton shapeButton = pluginButtons.get(i);
            shapeButton.setBounds(xOffset + buttonCellWidth*(i%3), yOffset + buttonCellHeight*(i/3), width, height);
            add(shapeButton);
        });
    }

    private void updatePlugins() {
        pluginButtons.forEach(pluginButton -> {
            pluginButton.setShape(editShapeListBackend.getCurrentShape());
            pluginButton.setShapes(editShapeListBackend.getShapeContainer().getShapes());
        });
    }

    private void resetCurrentShape() {
        setCurrentShape(0);
    }

    private void setCurrentShape(int index) {
        editShapeListBackend.setCurrentShapeIndex(index);
        shapeComboBox.setSelectedIndex(index);
        updatePlugins();
    }

    private void shapeComboBoxItemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            setCurrentShape(shapeComboBox.getSelectedIndex());
        }
    }

    private void editShapeButtonMousePressed(MouseEvent e) {
        final JDialog frame = new JDialog((JFrame) SwingUtilities.getWindowAncestor(this), "Edit shape", true);
        frame.getContentPane().add(new EditShapeForm(editShapeListBackend.getCurrentShape()));
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                editShapeListBackend.updateShapesPainted();
                resetShapeListComboBox();
                setCurrentShape(editShapeListBackend.getCurrentShapeIndex());
            }
        });
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void deleteShapeButtonMousePressed(MouseEvent e) {
        if (editShapeListBackend.getShapesPainted().isEmpty()) {
            return;
        }

        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to remove selected shape?", "Confirming shape removal", dialogButton);
        if(dialogResult == JOptionPane.YES_OPTION){
            editShapeListBackend.removeCurrentShape();
            shapeComboBox.removeItemAt(shapeComboBox.getSelectedIndex());
            if (!editShapeListBackend.getShapesPainted().isEmpty()) {
                resetCurrentShape();
            }
            JOptionPane.showMessageDialog(this, "Shape is deleted!");
        }
    }

    private void addShapeButtonMousePressed(MouseEvent e) {
        final JDialog frame = new JDialog((JFrame) SwingUtilities.getWindowAncestor(this), "Add new shape", true);
        frame.getContentPane().add(new NewShapeForm(new NewShapeBackend(editShapeListBackend.getShapeContainer())));
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                resetShapeListComboBox();
                resetCurrentShape();
            }
        });
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void resetShapeListComboBox() {
        editShapeListBackend.updateShapesPainted();
        shapeComboBox.setModel(new DefaultComboBoxModel(editShapeListBackend.getShapesPainted().toArray()));
    }

    private void saveChangesButtonMousePressed(MouseEvent e) {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to save shape list?", "Confirming shape list saving", dialogButton);
        if(dialogResult == JOptionPane.YES_OPTION){
            try {
                editShapeListBackend.saveShapeList();
                JOptionPane.showMessageDialog(this, "Shapes are saved!");
            } catch (IOException e1) {
                System.out.println("Failed to save shapes");
                JOptionPane.showMessageDialog(this,
                        "Failed to save shapes",
                        "Error while saving shapes",  JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
        shapeComboBox = new JComboBox();
        editShapeButton = new JButton();
        deleteShapeButton = new JButton();
        saveChangesButton = new JButton();
        addShapeButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(320, 285));
        setMaximumSize(new Dimension(320, 285));
        setPreferredSize(new Dimension(320, 285));

        setLayout(null);

        //---- shapeComboBox ----
        shapeComboBox.addItemListener(e -> shapeComboBoxItemStateChanged(e));
        add(shapeComboBox);
        shapeComboBox.setBounds(15, 15, 285, shapeComboBox.getPreferredSize().height);

        //---- editShapeButton ----
        editShapeButton.setText("Edit ");
        editShapeButton.setMaximumSize(new Dimension(90, 20));
        editShapeButton.setMinimumSize(new Dimension(90, 20));
        editShapeButton.setPreferredSize(new Dimension(90, 30));
        editShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                editShapeButtonMousePressed(e);
            }
        });
        add(editShapeButton);
        editShapeButton.setBounds(10, 60, 90, editShapeButton.getPreferredSize().height);

        //---- deleteShapeButton ----
        deleteShapeButton.setText("Delete");
        deleteShapeButton.setPreferredSize(new Dimension(90, 30));
        deleteShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                deleteShapeButtonMousePressed(e);
            }
        });
        add(deleteShapeButton);
        deleteShapeButton.setBounds(new Rectangle(new Point(110, 60), deleteShapeButton.getPreferredSize()));

        //---- saveChangesButton ----
        saveChangesButton.setText("Save changes");
        saveChangesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                saveChangesButtonMousePressed(e);
            }
        });
        add(saveChangesButton);
        saveChangesButton.setBounds(new Rectangle(new Point(100, 240), saveChangesButton.getPreferredSize()));

        //---- addShapeButton ----
        addShapeButton.setText("Add ");
        addShapeButton.setPreferredSize(new Dimension(90, 30));
        addShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addShapeButtonMousePressed(e);
            }
        });
        add(addShapeButton);
        addShapeButton.setBounds(210, 60, 90, addShapeButton.getPreferredSize().height);

        setPreferredSize(new Dimension(320, 285));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
    private JComboBox shapeComboBox;
    private JButton editShapeButton;
    private JButton deleteShapeButton;
    private JButton saveChangesButton;
    private JButton addShapeButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
