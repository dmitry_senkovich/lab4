/*
 * Created by JFormDesigner on Sun Mar 11 23:35:46 MSK 2018
 */

package com.proop.lab4.frontend;

import com.proop.lab4.backend.NewShapeListBackend;
import com.proop.lab4.backend.NewShapeBackend;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import static javax.swing.JFileChooser.APPROVE_OPTION;

/**
 * @author Dmitry Senkovich
 */
public class NewShapeListForm extends JPanel {

    private NewShapeBackend newShapeBackend = new NewShapeBackend();
    private NewShapeListBackend newShapeListBackend = new NewShapeListBackend();

    private NewShapeForm newShapeForm = new NewShapeForm(new NewShapeBackend(newShapeListBackend.getShapeContainer()));

    public NewShapeListForm() {
        initComponents();
        add(newShapeForm);
        // layout hack
        setBounds(0, 0, 400, 570);
        newShapeBackend.setShapeContainer(newShapeListBackend.getShapeContainer());
    }

    private void resetShapesButtonMousePressed(MouseEvent e) {
        resetCurrentShapes();
    }

    private void resetCurrentShapes() {
        newShapeForm.resetCurrentShape();
        newShapeListBackend.resetCurrentShapes();
        shapesTextArea.setText("");
    }

    private void showShapesButtonMousePressed(MouseEvent e) {
        shapesTextArea.setText(newShapeListBackend.drawShapes());
    }

    private void saveMousePressed(MouseEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showSaveDialog(this);
        if (APPROVE_OPTION == returnValue) {
            try {
                newShapeListBackend.saveShapes(fileChooser.getSelectedFile());
                resetCurrentShapes();
                JOptionPane.showMessageDialog(this, "Shapes are saved!");
            } catch (IOException e1) {
                System.out.println("Failed to save shapes");
                JOptionPane.showMessageDialog(this,
                        "Failed to save shapes",
                        "Error while saving shapes",  JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
        shapesScrollPane = new JScrollPane();
        shapesTextArea = new JTextArea();
        showShapesButton = new JButton();
        resetShapesButton = new JButton();
        saveButton = new JButton();
        setLayout(null);

        //======== shapesScrollPane ========
        {

            //---- shapesTextArea ----
            shapesTextArea.setEnabled(false);
            shapesTextArea.setEditable(false);
            shapesScrollPane.setViewportView(shapesTextArea);
        }
        add(shapesScrollPane);
        shapesScrollPane.setBounds(15, 400, 370, 80);

        //---- showShapesButton ----
        showShapesButton.setText("Show Shapes");
        showShapesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                showShapesButtonMousePressed(e);
            }
        });
        add(showShapesButton);
        showShapesButton.setBounds(145, 500, 105, 30);

        //---- resetShapesButton ----
        resetShapesButton.setText("Reset shapes");
        resetShapesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                resetShapesButtonMousePressed(e);
            }
        });
        add(resetShapesButton);
        resetShapesButton.setBounds(30, 500, 105, 30);

        //---- saveButton ----
        saveButton.setText("Save");
        saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                showShapesButtonMousePressed(e);
                saveMousePressed(e);
            }
        });
        add(saveButton);
        saveButton.setBounds(265, 500, 105, 30);

        setPreferredSize(new Dimension(400, 545));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
    private JScrollPane shapesScrollPane;
    private JTextArea shapesTextArea;
    private JButton showShapesButton;
    private JButton resetShapesButton;
    private JButton saveButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
