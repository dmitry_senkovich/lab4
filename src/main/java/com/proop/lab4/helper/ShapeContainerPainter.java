package com.proop.lab4.helper;

import com.proop.lab4.painter.ShapePainter;
import com.proop.lab4.painter.factory.ShapePainterFactory;
import com.proop.lab4.shape.Shape;

import java.util.List;
import java.util.stream.Collectors;

public class ShapeContainerPainter {

    private ShapePainterFactory shapePainterFactory = ShapePainterFactory.getInstance();

    public String drawShapes(ShapeContainer shapeContainer) {
        return shapeContainer.getShapes().stream().map(this::drawShape).collect(Collectors.joining(", "));
    }

    public List<String> drawEachShape(ShapeContainer shapeContainer) {
        return shapeContainer.getShapes().stream().map(this::drawShape).collect(Collectors.toList());
    }

    private String drawShape(Shape shape) {
        ShapePainter shapePainter = shapePainterFactory.getFactoryObject(shape.getClass());
        return shapePainter.paint(shape.getCoordinates());
    }

}
