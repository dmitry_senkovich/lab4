package com.proop.lab4.helper;

import com.proop.lab4.shape.Shape;

import java.util.ArrayList;
import java.util.List;

public class ShapeContainer {

    private List<Shape> shapes = new ArrayList<>();

    public void add(Shape shape) {
        shapes.add(shape);
    }

    public List<Shape> getShapes() {
        return shapes;
    }

    public void setShapes(List<Shape> shapes) {
        this.shapes = shapes;
    }
}
