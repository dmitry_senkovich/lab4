package com.proop.lab4.painter;

public class SegmentPainter implements ShapePainter {

    @Override
    public String paint(Integer[] coordinates) {
        return String.format("Segment((%d, %d), (%d, %d))",
                coordinates[0], coordinates[1], coordinates[2], coordinates[3]);
    }

}
