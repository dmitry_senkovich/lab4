package com.proop.lab4.painter.factory;

import com.proop.lab4.abstractfactory.AbstractFactory;
import com.proop.lab4.painter.ShapePainter;
import com.proop.lab4.shape.Shape;

import java.util.HashMap;
import java.util.Map;

public class ShapePainterFactory implements AbstractFactory<ShapePainter> {

    static private Map<Class<? extends Shape>, ShapePainter> shapePainters = new HashMap<>();
    static private ShapePainterFactory SHAPE_PAINTER_FACTORY = new ShapePainterFactory();

    private ShapePainterFactory() {}

    @Override
    public void registerShape(Class<? extends Shape> shapeClass, ShapePainter shapePainter) {
        shapePainters.put(shapeClass, shapePainter);
    }

    @Override
    public ShapePainter getFactoryObject(Class<? extends Shape> shapeClass) {
        return shapePainters.get(shapeClass);
    }

    public static ShapePainterFactory getInstance() {
        return SHAPE_PAINTER_FACTORY;
    }

}
