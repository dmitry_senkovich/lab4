package com.proop.lab4.painter;

public class EllipsePainter implements ShapePainter {

    @Override
    public String paint(Integer[] coordinates) {
        return String.format("Ellipse((%d, %d), %d, %d)",
                coordinates[0], coordinates[1], coordinates[2], coordinates[3]);
    }

}
