package com.proop.lab4.painter;

public class TrianglePainter implements ShapePainter {

    @Override
    public String paint(Integer[] coordinates) {
        return String.format("Triangle((%d, %d), (%d, %d), (%d, %d))",
                coordinates[0], coordinates[1], coordinates[2], coordinates[3], coordinates[4], coordinates[5]);
    }
}
