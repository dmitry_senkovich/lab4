package com.proop.lab4.painter;

public class CirclePainter implements ShapePainter {

    @Override
    public String paint(Integer[] coordinates) {
        return String.format("Circle((%d, %d), %d)",
                coordinates[0], coordinates[1], coordinates[2]);
    }

}
