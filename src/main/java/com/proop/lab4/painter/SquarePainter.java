package com.proop.lab4.painter;

public class SquarePainter implements ShapePainter {

    @Override
    public String paint(Integer[] coordinates) {
        return String.format("Square((%d, %d), %d)",
                coordinates[0], coordinates[1], coordinates[2]);
    }
}
