package com.proop.lab4.abstractfactory;

import com.proop.lab4.shape.Shape;

public interface AbstractFactory<F> {

    void registerShape(Class<? extends Shape> shapeClass, F factoryObject);

    F getFactoryObject(Class<? extends Shape> shapeClazz);

}
